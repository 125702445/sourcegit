﻿<controls:Window 
    x:Class="SourceGit.Views.Clone"
    x:Name="me"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:controls="clr-namespace:SourceGit.Views.Controls"
    xmlns:validations="clr-namespace:SourceGit.Views.Validations"
    mc:Ignorable="d"
    WindowStartupLocation="CenterOwner"
    Title="{DynamicResource Text.Clone}" 
    Width="500" SizeToContent="Height"
    ResizeMode="NoResize">

    <Window.TaskbarItemInfo>
        <TaskbarItemInfo ProgressState="None"/>
    </Window.TaskbarItemInfo>
    
    <Grid>
        <Grid.RowDefinitions>
            <RowDefinition Height="28"/>
            <RowDefinition Height="1"/>
            <RowDefinition Height="*"/>
        </Grid.RowDefinitions>

        <!-- Title bar -->
        <Grid Grid.Row="0" Background="{DynamicResource Brush.TitleBar}">
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="Auto"/>
                <ColumnDefinition Width="Auto"/>
                <ColumnDefinition Width="*"/>
                <ColumnDefinition Width="Auto"/>
            </Grid.ColumnDefinitions>

            <!-- Icon -->
            <Path Grid.Column="0" Margin="6,0" Width="16" Height="16" Data="{StaticResource Icon.Pull}"/>

            <!-- Title -->
            <TextBlock Grid.Column="1" Text="{DynamicResource Text.Clone}"/>

            <!-- Close -->
            <controls:IconButton
                Grid.Column="3"
                Click="OnQuit"
                Width="28"
                IconSize="10" 
                Icon="{StaticResource Icon.Close}" 
                HoverBackground="Red" 
                WindowChrome.IsHitTestVisibleInChrome="True"/>
        </Grid>

        <Rectangle 
            Grid.Row="1"
            Height="1"
            HorizontalAlignment="Stretch"
            Fill="{DynamicResource Brush.Border0}"/>

        <Grid Grid.Row="2">
            <!-- Body -->
            <Grid Margin="8">
                <Grid.RowDefinitions>
                    <RowDefinition Height="32"/>
                    <RowDefinition x:Name="rowSSHKey" Height="0"/>
                    <RowDefinition Height="32"/>
                    <RowDefinition Height="32"/>
                    <RowDefinition Height="32"/>
                    <RowDefinition Height="32"/>
                    <RowDefinition Height="48"/>
                </Grid.RowDefinitions>

                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="120"/>
                    <ColumnDefinition Width="*"/>
                </Grid.ColumnDefinitions>

                <TextBlock
                    Grid.Row="0" Grid.Column="0" 
                    Text="{DynamicResource Text.Clone.RemoteURL}"
                    Margin="0,0,4,0"
                    HorizontalAlignment="Right"/>
                <controls:TextEdit 
                    Grid.Row="0" Grid.Column="1"
                    x:Name="txtUrl"
                    Height="24"
                    Placeholder="{DynamicResource Text.Clone.RemoteURL.Placeholder}"
                    TextChanged="OnUrlChanged">
                    <controls:TextEdit.Text>
                        <Binding ElementName="me" Path="Uri" UpdateSourceTrigger="PropertyChanged" Mode="TwoWay">
                            <Binding.ValidationRules>
                                <validations:GitURL/>
                            </Binding.ValidationRules>
                        </Binding>
                    </controls:TextEdit.Text>
                </controls:TextEdit>

                <TextBlock
                    Grid.Row="1" Grid.Column="0" 
                    Text="{DynamicResource Text.SSHKey}"
                    Margin="0,0,4,0"
                    HorizontalAlignment="Right"/>
                <Grid Grid.Row="1" Grid.Column="1">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="*"/>
                        <ColumnDefinition Width="30"/>
                    </Grid.ColumnDefinitions>

                    <controls:TextEdit 
                        Grid.Column="0"
                        x:Name="txtSSHKey"
                        Height="24"
                        Placeholder="{DynamicResource Text.SSHKey.Placeholder}"/>

                    <controls:IconButton
                        Grid.Column="1"
                        Click="OnSelectSSHKey"
                        Width="24" Height="24"
                        Margin="2,0,0,0" IconSize="16"
                        BorderBrush="{DynamicResource Brush.Border1}"
                        BorderThickness="1"
                        Icon="{StaticResource Icon.Folder.Open}"/>
                </Grid>

                <TextBlock
                    Grid.Row="2" Grid.Column="0" 
                    Text="{DynamicResource Text.Clone.Folder}"
                    Margin="0,0,4,0"
                    HorizontalAlignment="Right"/>
                <Grid Grid.Row="2" Grid.Column="1">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="*"/>
                        <ColumnDefinition Width="30"/>
                    </Grid.ColumnDefinitions>

                    <controls:TextEdit
                        Grid.Column="0"
                        x:Name="txtFolder"
                        Height="24"
                        Placeholder="{DynamicResource Text.Clone.Folder.Placeholder}">
                        <controls:TextEdit.Text>
                            <Binding ElementName="me" Path="Folder" UpdateSourceTrigger="PropertyChanged" Mode="TwoWay">
                                <Binding.ValidationRules>
                                    <validations:CloneDir/>
                                </Binding.ValidationRules>
                            </Binding>
                        </controls:TextEdit.Text>
                    </controls:TextEdit>

                    <controls:IconButton
                        Grid.Column="1"
                        Click="OnFolderSelectorClick"
                        Width="24" Height="24"
                        Margin="2,0,0,0" IconSize="16"
                        BorderBrush="{DynamicResource Brush.Border1}"
                        BorderThickness="1"
                        Icon="{StaticResource Icon.Folder.Open}"/>
                </Grid>

                <TextBlock
                    Grid.Row="3" Grid.Column="0" 
                    Text="{DynamicResource Text.Clone.LocalName}"
                    Margin="0,0,4,0"
                    HorizontalAlignment="Right"/>
                <controls:TextEdit 
                    Grid.Row="3" Grid.Column="1"
                    Height="24"
                    x:Name="txtLocal"
                    Placeholder="{DynamicResource Text.Clone.LocalName.Placeholder}">
                    <controls:TextEdit.Text>
                        <Binding ElementName="me" Path="LocalName" UpdateSourceTrigger="PropertyChanged" Mode="TwoWay">
                            <Binding.ValidationRules>
                                <validations:LocalRepositoryName/>
                            </Binding.ValidationRules>
                        </Binding>
                    </controls:TextEdit.Text>
                </controls:TextEdit>

                <TextBlock
                    Grid.Row="4" Grid.Column="0" 
                    Text="{DynamicResource Text.Clone.RemoteName}"
                    Margin="0,0,4,0"
                    HorizontalAlignment="Right"/>
                <controls:TextEdit 
                    Grid.Row="4" Grid.Column="1"
                    x:Name="txtRemote"
                    Height="24"
                    Placeholder="{DynamicResource Text.Clone.RemoteName.Placeholder}">
                    <controls:TextEdit.Text>
                        <Binding ElementName="me" Path="RemoteName" UpdateSourceTrigger="PropertyChanged" Mode="TwoWay">
                            <Binding.ValidationRules>
                                <validations:RemoteName x:Name="ruleRemote"/>
                            </Binding.ValidationRules>
                        </Binding>
                    </controls:TextEdit.Text>
                </controls:TextEdit>

                <TextBlock
                    Grid.Row="5" Grid.Column="0" 
                    Text="{DynamicResource Text.Clone.AdditionalParam}"
                    Margin="0,0,4,0"
                    HorizontalAlignment="Right"/>
                <controls:TextEdit 
                    Grid.Row="5" Grid.Column="1"
                    Height="24"
                    Placeholder="{DynamicResource Text.Clone.AdditionalParam.Placeholder}"
                    Text="{Binding ElementName=me, Path=ExtraArgs, Mode=TwoWay}"/>

                <StackPanel 
                    Grid.Row="6" Grid.Column="0" Grid.ColumnSpan="2"
                    Height="32"
                    Orientation="Horizontal" 
                    HorizontalAlignment="Right" VerticalAlignment="Center">
                    <Button Click="OnStart" Width="80" Content="{DynamicResource Text.Start}" BorderBrush="{DynamicResource Brush.FG1}" Background="{DynamicResource Brush.Accent1}" FontWeight="Bold"/>
                    <Button Click="OnQuit" Width="80" Margin="8,0,0,0" Content="{DynamicResource Text.Cancel}" FontWeight="Bold"/>
                </StackPanel>
            </Grid>

            <!-- Progress -->
            <Border x:Name="progress" Visibility="Collapsed" Background="{DynamicResource Brush.Popup}" Opacity=".9">
                <StackPanel Orientation="Vertical" HorizontalAlignment="Center" VerticalAlignment="Center">
                    <controls:Loading x:Name="processing" Width="48" Height="48"/>
                    <TextBlock x:Name="txtProgress" Margin="0,16,0,0"/>
                </StackPanel>
            </Border>
            
            <!-- Exception -->
            <Grid x:Name="exception" Margin="8" Visibility="Collapsed" Background="{DynamicResource Brush.Window}">
                <Grid.RowDefinitions>
                    <RowDefinition Height="26"/>
                    <RowDefinition Height="*"/>
                    <RowDefinition Height="48"/>
                </Grid.RowDefinitions>

                <TextBlock 
                    Grid.Row="0" 
                    Text="{DynamicResource Text.Launcher.Error}" 
                    FontWeight="Bold"/>
                <controls:TextEdit
                    Grid.Row="1"
                    x:Name="txtError"
                    IsReadOnly="true"
                    BorderThickness="0"
                    TextWrapping="Wrap"
                    VerticalScrollBarVisibility="Auto"
                    MaxHeight="80"
                    Margin="0,8"
                    VerticalAlignment="Top"/>
                <Button 
                    Grid.Row="2" 
                    Height="26"
                    Margin="4,0" Padding="8,0" 
                    Click="OnCloseException"
                    Content="{DynamicResource Text.Sure}"
                    Background="{DynamicResource Brush.Accent1}"
                    BorderBrush="{DynamicResource Brush.FG1}"
                    BorderThickness="1"
                    HorizontalAlignment="Right"/>
            </Grid>
        </Grid>
    </Grid>
</controls:Window>
